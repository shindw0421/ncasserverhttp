#!/bin/sh
echo "##### 사용법 #####"
echo "시작 : ./casSimul start"
echo "종료 : ./casSimul stop"
echo "재시작 : ./casSimul restart"
echo "##### 처리 상태 #####"

SERVICE_NAME=casSumul
PATH_TO_JAR=/svc/stkota/was/app/casSimul/nCasServer-0.0.1-SNAPSHOT.jar
PID_PATH_NAME=/svc/stkota/was/app/casSimul/casSimul-pid
JAVA_OPTS="-server -Dspring.profiles.active=server"
case $1 in
	start)
		echo "Starting $SERVICE_NAME ..."
		if [ ! -f $PID_PATH_NAME ]; then
			nohup java $JAVA_OPTS -jar $PATH_TO_JAR /tmp 2>> /dev/null >> /dev/null &
				echo $! > $PID_PATH_NAME
			echo "$SERVICE_NAME started ..."
		else
			echo "$SERVICE_NAME is already running ..."
		fi
	;;
	stop)
		if [ -f $PID_PATH_NAME ]; then
			PID=$(cat $PID_PATH_NAME);
			echo "$SERVICE_NAME stoping ..."
			kill $PID;
			echo "$SERVICE_NAME stopped ..."
			rm $PID_PATH_NAME
		else
			echo "$SERVICE_NAME is not running ..."	
		fi
	;;
	restart)
		if [ -f $PID_PATH_NAME ]; then
			PID=$(cat $PID_PATH_NAME);
			echo "$SERVICE_NAME stoping ..."
			kill $PID;
			echo "$SERVICE_NAME stopped ..."
			rm $PID_PATH_NAME
			echo "$SERVICE_NAME starting ..."
			nohup java $JAVA_OPTS -jar $PATH_TO_JAR /tmp 2>> /dev/null >> /dev/null &
				echo $! > $PID_PATH_NAME
			echo "$SERVICE_NAME started ..."
		else
			echo "$SERVICE_NAME is already running ..."
		fi
	;;
esac