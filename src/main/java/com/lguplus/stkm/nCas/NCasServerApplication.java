package com.lguplus.stkm.nCas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class NCasServerApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(NCasServerApplication.class, args);
	}

}
