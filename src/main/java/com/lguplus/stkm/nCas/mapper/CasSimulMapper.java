package com.lguplus.stkm.nCas.mapper;

import java.util.HashMap;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CasSimulMapper {
	HashMap<String, String> getUiccid();
}
