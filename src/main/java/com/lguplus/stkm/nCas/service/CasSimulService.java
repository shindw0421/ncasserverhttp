package com.lguplus.stkm.nCas.service;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lguplus.stkm.nCas.mapper.CasSimulMapper;

@Service
@Transactional
public class CasSimulService {
	
	@Autowired
	CasSimulMapper casSimulMapper;
	
	public HashMap<String, String> getUiccid() {
		return casSimulMapper.getUiccid();
	}
}
