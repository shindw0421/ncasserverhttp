package com.lguplus.stkm.nCas.controller;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lguplus.stkm.nCas.service.CasSimulService;

@RestController
@RequestMapping("")
public class CasSimulController {

	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private CasSimulService casSimulService;
	
	@RequestMapping(value = {"/cas"}
		, method=RequestMethod.GET
		, headers="Accept=application/json"
		, produces = "text/plain;charset=UTF-8")
	public ResponseEntity<String> casSimul( 
			@RequestParam(value="CTN", required=true, defaultValue="") String ctn
			) throws Exception {
		
		HashMap<String, String> result = casSimulService.getUiccid();
		
		StringBuilder sb = new StringBuilder();
		sb.append("RESPCODE=00");
		sb.append("&RESPMSG=%EC%A0%95%EC%83%81");
		sb.append("&UNIT_MDL_ORG=").append(result.get("SECHIPMODELNM"));
		sb.append("&CUST_TYPE_CODE=G");
		sb.append("&CTN_STUS_CODE=A");
		sb.append("&USIM_ICCID_NO=").append(result.get("UICCID"));
		sb.append("&UNIT_MDL=").append(result.get("SECHIPMODELNM"));
		sb.append("&CTN=").append(ctn);
		
		logger.debug("##CAS SIMUL ==> {}", sb.toString());
		
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.set("RESP", sb.toString());
		
		return ResponseEntity.ok()
				.headers(responseHeaders)
				.body(sb.toString());
		
	}

}
